import { useState } from "react"
import Card, {params as paramsCard} from "./components/Card"
import Grid from "./components/Grid"

const App = (): JSX.Element => {

  const [data, setData] = useState<paramsCard[]>([
    {value:"1", image:"/vite.svg", title: "Tienda"},
    {value:"2", image:"/vite.svg", title: "Tienda"},
    {value:"3", image:"/vite.svg", title: "Tienda"},
    {value:"4", image:"/vite.svg", title: "Tienda"},
    {value:"5", image:"/vite.svg", title: "Tienda"},
    {value:"6", image:"/vite.svg", title: "Tienda"},
    {value:"7", image:"/vite.svg", title: "Tienda"}
  ])

  const handleSelect: paramsCard['onClick'] = (e) =>{
    console.log(e)
  }
  return (
    <Grid>
      { data.map((v,i)=><Card key={i} {...v} onClick={handleSelect}/>)}
    </Grid>
  )
}

export default App
