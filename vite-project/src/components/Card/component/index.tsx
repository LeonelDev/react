import styled from "styled-components"
import Avatar from "../../Avatar"

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 30px 15px;
  background-color: red;
  width: 123px;
  height: 160px;
  border-radius: 6px;
`
const Title = styled.p`
  color: white;
`

const Description = styled.p`
  font-size: 12px;
`

export interface params{
  value: string
  title: string
  description?: string
  image: string
  onClick?:(value: string)=>void
}

/**
 * 
 * @param params
 * @param params.value Identificador del elemento
 * @param params.title title del elemento
 * @param params.description description del elemento
 * @param params.image avatar del elemento
 * @param params.onClick callback que retorna un identificador al presionar sobre el componente
 * 
 * @example
 * import { useState } from "react"
import Card, {params as paramsCard} from "./components/Card"
import Grid from "./components/Grid"

const App = (): JSX.Element => {

  const [data, setData] = useState<paramsCard[]>([
    {value:"1", image:"/vite.svg", title: "Tienda"},
    {value:"2", image:"/vite.svg", title: "Tienda"},
    {value:"3", image:"/vite.svg", title: "Tienda"}
  ])
  return (
    <Grid>
      { data.map((v,i)=><Card key={i} {...v} onClick={}/>)}
    </Grid>
  )
}
 * 
 * @return JSX.Element 
*/
const App = (params: params): JSX.Element => {

  const handleClick = ()=>{
    if(typeof params.onClick === "function") params.onClick(params.value)
  }
  return (
    <Container onClick={handleClick}>
      <Avatar image={params.image}/>
      <Title>{params.title}</Title>
      <Description>{params.description}</Description>
      
    </Container>
  )
}

export default App
