import React from 'react';
import styled from 'styled-components';

import Options, { Circulo, Icon } from './options';

import check from '../images/check.jpg';
import cancel from '../images/cancel.jpg';

const Container = styled.div`
    background-color: #21d0d0;
    border-radius: 25px;
    height: 200px;
    width: 350px;
    opacity: 0.8;
    padding-top: 15px;
  `;

const Footer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  
`;
const App = () => {

    const options = [
        { title: "PRICE LOW TO HIGH", state: false},
        { title: "PRICE HOGH TO LOW",  state: false},
        { title: "PUPOLARITY", state: true}
    ]
  //{...v} -> significa que se va a pasar todas la propiedades del valor
  return (
    <Container >
        {options.map((v, i)=><Options key={i} {...v}/>)} 

        <Footer>
          {
            [cancel, check].map((v,i)=>{
              return(
                <Circulo visible={true} style={{ marginLeft: 15,height: 60, width: 60, boxShadow:"0px 2px 5px 1px #592A08"}}>
                  <Icon src={v} visible={true} style={{ height: 60, width: 60}}/>
                </Circulo>
              )
            })
          }
        </Footer>
    </Container>
  );
}

export default App;
