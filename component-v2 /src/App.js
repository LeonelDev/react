import React, { useState } from 'react';
import styled from 'styled-components';

import Card from './Card';
import Submenu from './submenu';

const Container = styled.div`
    
  `;

const App = () => {
  const [data, setData] = useState([
    { id: 1,
      color: "#21d0d0",
      options: [
        { title: "PRICE LOW TO HIGH", state: false},
        { title: "PRICE HOGH TO LOW",  state: false},
        { title: "PUPOLARITY", state: true}
      ],
      version: 1 
    },
    { id: 2,
      color: "#f06f37",
      options: [
        { title: "1UP NUTRIRION", state: false},
        { title: "ASITIS",  state: false},
        { title: "AVVATAR", state: true},
        { title: "BIG MUSCLES", state: true},
        { title: "BPI SPORTS", state: true},
        { title: "BSN", state: true},
        { title: "CELLUCOR", state: true},
        { title: "DOMIN8R", state: true}
      ],
      version: 2  
    }
  ])

  const handleChange = (value, id) =>{
    const match = data.filter(el => el.id === id)[0];
    const act = match.options.filter(el => el.title === value)[0];
    const copy = [...match.options];

    //find index of item to be replaced
    const targetIndex = copy.findIndex(f => f.title === value);
    if(targetIndex > -1){
      //replace the object with a new one
      copy[targetIndex]= {title: value, state: !act.state};

      match.options = copy;
    }

    const copyList = [...data];

    //find index of item to be replaced
    const targetIndexList = copyList.findIndex(f => f.title === id);
    if(targetIndexList > -1){
      //replace the object with a new one
      copy[targetIndexList]= match;
       
    }
    // console.log(copyList);
    setData(copyList);

  }
  const handleReset = (id) => {
    const copy = [...data];

    //find index of item to be replaced
    const targetIndex = copy.findIndex(f => f.id === id);
    if(targetIndex > -1){
      var raw = [];
      const options = data.filter(el => el.id === id)[0].options;

      for(let x of options) raw.push({title: x.title, state: false})
      //replace the object with a new one
      copy[targetIndex].options = raw;

    }
    setData(copy)
  }
  return (
    <Container >
      <Card />
      {data.map((v, i)=> <Submenu key={i} {...v} onChange={handleChange} onReset={handleReset}/>)}
      
    </Container>
  );
}

export default App;
