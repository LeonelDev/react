import React from "react";
import styled from "styled-components";

import { Circulo, Icon } from "./options";

import check from "../images/check.jpg";
import cancel from "../images/cancel.jpg";
import reset from "../images/reset.png";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;
const ContainerV1 = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-top: 15px;
  padding-top: 25px;
  padding-bottom: 25px;
  border-top: "1px solid #ffffff80";
`;

const Item = ({ v, onclick }) => {
  return (
    <Circulo
      visible={true}
      style={{
        marginLeft: 15,
        height: 60,
        width: 60,
        boxShadow: "0px 2px 5px 1px #592A08",
      }}
      onClick={onclick}
    >
      <Icon src={v} visible={true} style={{ height: 60, width: 60 }} />
    </Circulo>
  );
};

const App = ({ version, onReset, onCancel, onAcept }) => {
  const handleReset = () => {
    console.log("vamos a resetear");
    if(typeof onReset === 'function') onReset()
  };
  const handleOk = () => {
    console.log("vamos a Aceptar");
    if(typeof onAcept === 'function') onAcept()
  };
  const handleCancel = () => {
    console.log("vamos a cancelar");
    if(typeof onCancel === 'function') onCancel()
  };

  
  if (version === 1) {
    let data = [
      { icon: cancel, onclick: handleCancel },
      { icon: check, onclick: handleOk },
    ];
    return (
      <Container>
        {data.map((v, i) => (
          <Item key={i} v={v.icon} onclick={v.onclick} />
        ))}
      </Container>
    );
  }
  if (version === 2) {
    let data = [
      { icon: cancel, onclick: handleCancel },
      { icon: reset, onclick: handleReset },
      { icon: check, onclick: handleOk },
    ];
    return (
      <ContainerV1>
        {data.map((v, i) => (
          <Item key={i} v={v.icon} onclick={v.onclick}/>
        ))}
      </ContainerV1>
    );
  }
};

export default App;
