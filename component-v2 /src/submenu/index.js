import React from 'react';
import styled from 'styled-components';

import Options from './options';
import Footer from './footer';

const Container = styled.div`
    margin-top: 10px;
    background-color: ${props => props.color};
    border-radius: 25px;
    /* height: 200px; */
    width: 350px;
    opacity: 0.8;
    padding-top: 15px;
  `;


const App = (params) => {

  /**
   * color: string
   * options: string[]
   */

  //{...v} -> significa que se va a pasar todas la propiedades del valor
  const handleChange = (value) => {
    if(typeof params.onChange === 'function') params.onChange(value, params.id);
  }
  const handleReset = (value) => {
    if(typeof params.onReset === 'function') params.onReset(params.id);
  }
  return (
    <Container color={params.color}>
        {params.options.map((v, i)=><Options key={i} {...v} onChange={handleChange}/>)} 

        <Footer version={params.version} onReset={handleReset}/>
    </Container>
  );
}

export default App;
