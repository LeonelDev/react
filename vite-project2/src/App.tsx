import { useState } from "react"
import Card, {params as paramsCard} from "@react/card"
import CardProduct from "@react/product"
import Grid from "@react/grid"

const App = (): JSX.Element => {

  const [data, setData] = useState<paramsCard[]>([
    {value:"1", image:"/vite.svg", title: "Tienda"},
    {value:"2", image:"/vite.svg", title: "Tienda"},
    {value:"3", image:"/vite.svg", title: "Tienda"},
    {value:"4", image:"/vite.svg", title: "Tienda"},
    {value:"5", image:"/vite.svg", title: "Tienda"},
    {value:"6", image:"/vite.svg", title: "Tienda"},
    {value:"7", image:"/vite.svg", title: "Tienda"}
  ])

  const handleSelect: paramsCard['onClick'] = (e) =>{
    console.log(e)
  }
  return (
    <Grid>
      <CardProduct 
        value="1" 
        image="https://oechsle.vteximg.com.br/arquivos/ids/9126530-1500-1500/2148233.jpg?v=637905029414570000" 
        title="Telefono"
        onMouseOver={()=> console.log("Over")}
        onMouseLeave={()=> console.log("Leave")}
        />
      <CardProduct value="2" image="https://oechsle.vteximg.com.br/arquivos/ids/9126530-1500-1500/2148233.jpg?v=637905029414570000" title="Telefono"></CardProduct>
      { data.map((v,i)=><Card key={i} {...v} onClick={handleSelect}/>)}
    </Grid>
  )
}

export default App
