import React from "react";
import styled from "styled-components";

const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 150px;    
`
const Img = styled.img`
    aspect-ratio: 9/16;
    object-fit: cover;
`
const Body = styled.div`
    display: flex;
    flex-direction: column;
    padding-left: 10px;
    padding-right: 10px;
    background-color: #1c1719;
    border-radius: 5px;
`
const Title = styled.p`
    color: white;
`
const Description = styled.p`
    
`
export interface params{
    value: string
    image: string
    title: string
    description?:string
    onClick?: (value:string)=>void
    onMouseLeave?: React.MouseEventHandler<HTMLDivElement>
    onMouseOver?: React.MouseEventHandler<HTMLDivElement> 
}

const defaultProps = {
    //image: "https://oechsle.vteximg.com.br/arquivos/ids/9126530-1500-1500/2148233.jpg?v=637905029414570000",
    //title: "Telefono"
}

/**
 * 
 * @param params
 * @param params.value key of product
 * @param params.image image of profuct
 * @param params.title title of product
 * @param params.description description of product
 * 
 * @example
import CardProduct from "@react/product"

const App = (): JSX.Element => {

  return (
      <CardProduct 
        value="1" 
        image="https://oechsle.vteximg.com.br/arquivos/ids/9126530-1500-1500/2148233.jpg?v=637905029414570000" 
        title="Telefono"
        onMouseOver={()=> console.log("Over")}
        onMouseLeave={()=> console.log("Leave")}
        />
  )
}

export default App

 * @returns JSX.Element
 */
const App = (params: params): JSX.Element => {

    params = {...defaultProps, ...params}

    const handleClick = () =>{
        if(typeof params.onClick === "function") params.onClick(params.value)
    }

    return(
        <Container onClick={handleClick} onMouseLeave={params.onMouseLeave} onMouseOver={params.onMouseOver}>
            <Img src={params.image}/>
            <Body>
                <Title>{params.title}</Title>
                <Description>{params.description}</Description>
            </Body>
        </Container>
    )
}

export default App