"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const styled_components_1 = __importDefault(require("styled-components"));
const avatar_1 = __importDefault(require("@react/avatar"));
const Container = styled_components_1.default.div `
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 30px 15px;
  background-color: red;
  width: 123px;
  height: 160px;
  border-radius: 6px;
`;
const Title = styled_components_1.default.p `
  color: white;
`;
const Description = styled_components_1.default.p `
  font-size: 12px;
`;
const App = (params) => {
    const handleClick = () => {
        if (typeof params.onClick === "function")
            params.onClick(params.value);
    };
    return (react_1.default.createElement(Container, { onClick: handleClick },
        react_1.default.createElement(avatar_1.default, { image: params.image }),
        react_1.default.createElement(Title, null, params.title),
        react_1.default.createElement(Description, null, params.description)));
};
exports.default = App;
