/// <reference types="react" />
export interface params {
    value: string;
    title: string;
    description?: string;
    image: string;
    onClick?: (value: string) => void;
}
declare const App: (params: params) => JSX.Element;
export default App;
