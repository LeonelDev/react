# [Template](http://casesandberg.github.io/react-color/)

## Installation & Usage

```sh
npm install @react/card --save
```

### Include the Component

```js
import App from '@react/card'

const App = () => {
    return <App />
}
```