# [Template](http://casesandberg.github.io/react-color/)

## Installation & Usage

```sh
npm install @react/avatar --save
```

### Include the Component

```js
import App from '@react/avatar'

const App = () => {
    return <App />
}
```