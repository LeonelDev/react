import React from "react";
import { CSSProperties } from "styled-components";
export interface params {
    children: React.ReactNode;
    style?: CSSProperties;
}
declare const App: (params: params) => JSX.Element;
export default App;
