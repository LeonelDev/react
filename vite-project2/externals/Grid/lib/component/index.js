"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const styled_components_1 = __importDefault(require("styled-components"));
const Container = styled_components_1.default.div `
    position: relative;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-self: center;
    align-content: flex-start;
    display: flex;
    width: 100%;
    height: 100%;
    overflow: scroll;
    gap: 15px;
`;
const App = (params) => {
    return (react_1.default.createElement(Container, { style: params.style }, params.children));
};
exports.default = App;
