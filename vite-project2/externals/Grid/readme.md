# [Template](http://casesandberg.github.io/react-color/)

## Installation & Usage

```sh
npm install @react/grid --save
```

### Include the Component

```js
import App from '@react/grid'

const App = () => {
    return <App />
}
```