import React from "react";
import styled, { CSSProperties } from "styled-components";

const Container = styled.div`
    position: relative;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-self: center;
    align-content: flex-start;
    display: flex;
    width: 100%;
    height: 100%;
    overflow: scroll;
    gap: 15px;
`
export interface params {
    children: React.ReactNode
    style?: CSSProperties
}

/**
 * 
 * @param params
 * @param params.children children of grid
 * @param params.style style of grid
 * @example
 * import Grid from '@eact/product" 
 * 
 * const App = () => {
 *  return (
 *      <Grid>
 *          
 *      </Grid>
 *  )
 * }
 * export default App
 * @returns JSX.Element 
 */

const App = (params:params): JSX.Element => {
    return(
        <Container style={params.style}>
            {params.children}
        </Container>
    )
}
export default App