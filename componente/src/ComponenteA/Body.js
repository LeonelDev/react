import styled from 'styled-components';

const Container = styled.div`
    width: 75%;
    padding-left: 25px;
    display: flex;
    flex-direction: column;
    justify-content: center;
`;

const Title = styled.p`
    margin: 0;
    color: #405382;
    font-weight: bold;
    font-size: 15px;
`;

const Description = styled.p`
    margin: 0;
    margin-top: 5px;
    color: #828b92;
    font-size: 14px;
`;

const Enlace = styled.a`
    margin: 0;
    margin-top: 5px;
    color: #3b8699;
    font-size: 14px;
`;

const App = () =>{
    return(
        <Container>
            <Title>
                Paquete premium
            </Title>
            <Description>
                Descubre nuevas funciones
            </Description>
            <Enlace>
                Hazte premium
            </Enlace>
        </Container>
    )
}
export default App;