import React from 'react';
import styled from 'styled-components'
import ComponenteA from './ComponenteA'
import dibujo from './images/24235.png';

const Container = styled.div`
    
  `;

const App = () => {
  
  return (
    <Container >
      <ComponenteA icon ={dibujo} colorIcon={"#fff6e5"} />
      <ComponenteA icon ={dibujo} colorIcon={"#ebfcf1"} />
      <ComponenteA icon ={dibujo} colorIcon={"#f4f2ff"} />
    </Container>
  );
}

export default App;
