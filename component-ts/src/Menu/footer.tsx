import styled from "styled-components";
import Checkbox from './checkbox';

import check from "../images/check.jpg";
import cancel from "../images/cancel.jpg";
import reset from "../images/reset.png";
import { useEffect, useState } from "react";

const Container = styled.div`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
`;
const ContainerV1 = styled.div`
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
margin-top: 15px;
padding-top: 25px;
padding-bottom: 25px;
border-top: "1px solid #ffffff80";
`;

interface options {
  icon: string;
  onClick: () => void;
}

export interface params{ 
  enableReset: boolean;
  enableCancel: boolean;
}

const App = (params: params): JSX.Element => {

  const handleReset = () => {
    console.log("vamos a resetear");
    if(typeof onReset === 'function') onReset()
  };
  const handleOk = () => {
    console.log("vamos a Aceptar");
    if(typeof onAcept === 'function') onAcept()
  };
  const handleCancel = () => {
    console.log("vamos a cancelar");
    if(typeof onCancel === 'function') onCancel()
  };

  const data: options[] = [
    { icon: cancel, onClick: handleCancel },
    { icon: reset, onClick: handleReset },
    { icon: check, onClick: handleOk },
  ];
    return (
      <Container>
        {data.map((v, i) => (
          <Checkbox key={i} state={false} onClick={v.onClick}/>
        ))}
      </Container>
    )
  }
  
export default App
  