import styled from 'styled-components';

import check from '../images/check.jpg';
import Checkbox from './checkbox';

const Container = styled.div`
    margin-left: 25px;
    margin-top: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-right: 25px;
    align-items: center;
  `;

const Title = styled.p`
    margin: 0px;
    color: white;
    font-weight: 500;
    text-transform: uppercase;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
`;

const Circulo = styled.div<{ visible: boolean }>`
    border-radius: 50%;
    border: 1.8px solid white;
    height: 35px;
    width: 35px;
    cursor: pointer;
    background-color: ${props => props.visible ? 'white':'tranparent'};
`;

const Icon = styled.img<{ visible: boolean }>`
    height: 35px;
    width: 35px;
    object-fit: contain;
    border-radius: 50%;
    visibility: ${props => props.visible ? "visible":'hidden'};
`;

export interface params{
    value: string;
    onClick?: (params: {value: string, state: boolean})=>void;
}

const App = (params: params): JSX.Element => {

    const handleClick = (state: boolean) => {
        if(typeof params.onClick === "function") {
            params.onClick({value: params.value, state});
        }
    }

    return (
        <Container >
        <Title>
            {params.value}
        </Title>
        <Checkbox image={check} onClick={handleClick}/>
    </Container>
    )
  }
  
export default App
  