import { useState } from 'react';
import styled from 'styled-components';

const Container = styled.div<{ visible: boolean }>`
    border-radius: 50%;
    border: 1.8px solid white;
    height: 35px;
    width: 35px;
    cursor: pointer;
    background-color: ${props => props.visible ? 'white':'tranparent'};
`;

const Icon = styled.img<{ visible: boolean }>`
    height: 35px;
    width: 35px;
    object-fit: contain;
    border-radius: 50%;
    visibility: ${props => props.visible ? "visible":'hidden'};
`;

export interface params{
    image: string;
    onClick?: (value: boolean)=>void;
}

const App = (params: params): JSX.Element => {

    const [visible, setVisible] = useState<boolean>(false);

    const handleClick = () => {
        setVisible(!visible);

        if(typeof params.onClick === "function") params.onClick(!visible);
    }

    return (
        <Container visible={visible} onClick={handleClick}>
            <Icon src={params.image} visible={visible}/>
        </Container>
    )
  }
  
export default App
  