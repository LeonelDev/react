import styled from 'styled-components';
import Option, { params } from './Menu/option';

const Container = styled.div`
  background-color: red;
`;

const App = (): JSX.Element => {

  const options: string[] = [
     "1UP NUTRIRION", 
    "ASITIS", 
    "AVVATAR",
    "BIG MUSCLES",
    "BPI SPORTS", ,
    "BSN", 
    "CELLUCOR", 
    "DOMIN8R"
  ];
  const handleClick: params['onClick'] = (value ) => {
    console.log(value)
  }
  return (
    <Container >
      {options.map((v,i)=> <Option key={i} value={v} onClick={handleClick}/>)}
    </Container>
  )
}

export default App
